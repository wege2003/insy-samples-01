# Docker Cheat Sheet

## Overview

See https://www.docker.com/resources/what-container

![img](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)



## Coordinates

**Register** yourself at *https://www.docker.com/*

**Docker Hub**

- https://hub.docker.com/
- Online Repo for official, non-official and your own image

**Commercial Application of a dock and containers**

Valparaiso/Chile

![File:Valparaiso Port (Chile) - new.jpg](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/Valparaiso_Port_%28Chile%29_-_new.jpg/800px-Valparaiso_Port_%28Chile%29_-_new.jpg)



## Image

**List all images on a host**

```bash
docker image ls
```

**Remove an image**

```bash
docker image rm <name>
```

**Get an image from hub.docker.com**

```bash
docker pull repository/image:tag
```

**Get an official image**

```bash
docker pull image:tag
```

- repository: Name of the repo

- image: Name of the image

- tag: version (or *latest*) or other tag

### Container

**Run a container**

```bash
docker run -d -p hostport:containerport image
```

- Creates a container based on the image `image` and runs whatwever is defined in the image.

- Pulls the image if necessary.

- Creates a surprising fun name for this container

Example

```bash
docker run -d -p 80:80 --name nginx_basic nginx
```

- `-d`, `--detach` detach and run in background
- `-p hostport:containerport` when `containerport` is exposed on the container, it is mapped to `hostport` on the host. 
- `--name` name that shows up in `docker ps`. Must be unique. Avoids the surprising fun name
- `--rm` remove the container after it has been running.

**Run a command in a container**

see https://docs.docker.com/engine/reference/commandline/container_exec/

```bash
docker exec -it <container> <command>
```

Example: run `bash` inside the container to work inside the container.

```bash
docker exec -it nginx_basic /bin/bash
```

Options

- `-i` , `--interactive` interactive
- `-t`,  `--tty` keep a pseudoterminal open

**List all running containers**

```bash
docker ps
docker container ls
```

**List all containers, including stopped ones**

```bash
docker ps -a
docker ps --all

docker container ls -a
docker container ls --all
```

**Start a container** 

```bash
docker start <id>
docker start <name>
```

**Stop a container** 

```bash
docker stop <id>
docker stop <name>
```

**Remove a container**

```bash
docker container rm <id>|<name>
```

**Stop a container and remove it**

```bash
docker container rm -f <id>|<name>
docker container rm --force <id>|<name>
```

## Storage - Volumes & Bind Mounts

- **Volumes** are stored in a part of the host filesystem which is *managed by Docker* (`/var/lib/docker/volumes/` on Linux). Non-Docker processes should not modify this part of the filesystem. Volumes are the best way to persist data in Docker. 
- **Bind mounts** may be stored *anywhere* on the host system. They may even be important system files or directories. Non-Docker processes on the Docker host or a Docker container can modify them at any time.

### Volumes

![volumes on the Docker host](https://docs.docker.com/storage/images/types-of-mounts-volume.png)

**Create a volume**

```bash
docker volume create nginx_pages
```

Inspect a volume

```bash
docker volume inspect nginx_pages
[
    {
        "CreatedAt": "2020-02-23T16:00:46Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/myVolume/_data",
        "Name": "nginx_pages",
        "Options": {},
        "Scope": "local"
    }
]
```

Ah, it is inside `/var/lib/docker/volumes/`

**Mount a volume**

Volumes can be mounted via `-v` or `--mount`. The latter is recommended for new users but many samples use `-v`. `--mount` uses a clearer but more verbose syntax. 

```bash
docker run -d --name nginx_volume_v -v nginx_pages:/usr/share/nginx/html -p 80:80 nginx

docker run -d --name nginx_volume_mount \
              --mount source=nginx_pages,target=/usr/share/nginx/html -p 80:80 nginx
```

**Mount an anonymous volume**

```js
docker run -d --name nginx_volume_anonymous_v -v :/usr/share/nginx/html -p 80:80 nginx

docker run -d --name nginx_volume_anonymous_mount \
              --mount target=/usr/share/nginx/html -p 80:80 nginx
```

A short `docker volume ls` reveals that docker has automagically created anonymous volumes.

```bash
DRIVER              VOLUME NAME
local               61cdc8777814d3f03152b6d57fb64fbfa3558daed024b02187d95c043d11274e
local               c1723c601d2e34867333e0858db1b24717fcb40e50ff272ac36daf6193e55fac
local               nginx_pages
```

**Populate a volume using a container**

If you start a container which creates a new volume, and the container has files or directories in the directory to be mounted (such as `/usr/share/nhinx/html`), the directory’s contents are copied into the volume. The container then mounts and uses the volume, and other containers which use the volume also have access to the pre-populated content.

### Bind Mounts

A *bind mount* allows to mount a folder from the host into a container. This is might be handy for local development environments but not very useful for deployments:

- Bind mounts depend on a specific directory structure which might not be available somewehere else.
- Bind mounts 

![bind mounts on the Docker host](https://docs.docker.com/storage/images/types-of-mounts-bind.png)



```bash
docker run -d -p 80:80 -v "$(pwd)"/web:/usr/share/nginx/html --name nginx_bind_mount_v nginx

docker run -d -p 80:80 --mount type="bind",source="$(pwd)",target=/usr/share/nginx/html \
                       --name nginx_bind_mount nginx
```

`-v` creates the folder if it is not available, `--mount` does not create the folder and throws an error if it is not already there.

## Networking

Docker offers many options to connect containers to each other and to clients. Usually the choice is the network driver.

- **User-defined *bridge* networks** are best when you need multiple containers to communicate on the same Docker host. Default.
- ***Host* networks** are best when the network stack should not be isolated from the Docker host, but you want other aspects of the container to be isolated.
- ***Overlay* networks** are best when you need containers running on different Docker hosts to communicate, or when multiple applications work together using swarm services.
- ***Macvlan* networks** are best when you are migrating from a VM setup or need your containers to look like physical hosts on your network, each with a unique MAC address.
- **Third-party network plugins** allow you to integrate Docker with specialized network stacks.

#### Default bridge network

**List all networks**

```bash
docker network ls
```

Shows us a few networks

```bash
NETWORK ID          NAME                DRIVER              SCOPE
ac4bcaca1532        bridge              bridge              local
9991aad24c4d        host                host                local
e8f0f3b1cf8b        none                null                local
```

**Example**

https://docs.docker.com/network/network-tutorial-standalone/#use-the-default-bridge-network

**Show details of a network**

```bash
docker network inspect bridge
```

```bash
[
    {
        "Name": "bridge",
        "Id": "ac4bcaca153275a68d1c9ce721e39a1474e0ea6a8c8372003a2c72a44f4ae078",
        "Created": "2020-02-23T20:08:39.617154276+01:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "192.168.1.5/24",
                    "IPRange": "192.168.1.0/25",
                    "Gateway": "192.168.1.5"
                },
                {
                    "Subnet": "2001:db8::/64"
                }
            ]
        },
        ....
        "Containers": {
            "02286972f630cec957d250fdd868dada8dd2bac30c26e53998b2ecf62b43e90b": {
                "Name": "nginx_bind_mount",
                "EndpointID": "ee6bb37f275f3a1e...179ef29712b7a5601f3d4",
                "MacAddress": "02:42:c0:a8:01:01",
                "IPv4Address": "192.168.1.1/24",
                "IPv6Address": ""
            },
            .....
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]

```

**Create a container on the default bridge network**

```bash
docker run -dit --name alpine1  alpine1 ash
docker run -dit --name alpine2  alpine2 ash
```

**Attach to the container**

```bash
docker container attach alpine1
ifconfig ...
ping ...
....
<ctrl>-P <ctrl>-Q
```



### User-defined bridge networks

```bash
docker network create --driver bridge alpine-net
```

Don't omit `--driver bridge` as a network 172.18.0.0 might be created ...

In an user-defined-bridge network

- Containers can be accessed via their name (service discovery)
- Containers can talk to each other
- Containers cannot talk to containers on another user defined network

**Example**

https://docs.docker.com/network/network-tutorial-standalone/#use-user-defined-bridge-networks

**Create a container on the default bridge network**

```bash
docker run -dit --name alpine1  alpine1 ash
docker run -dit --name alpine2  alpine2 ash
```

**Attach to the container**

```bash
docker container attach alpine1
ifconfig ...
ping ...
....
<ctrl>-P <ctrl>-Q
```



## Build Images / Dockerfile

A great description can be found here: https://blog.hipolabs.com/understanding-docker-without-losing-your-shit-cf2b30307c63.

### Basic Sample

We build a very simple app – a helloworld shell script and save it on the host in `./app/hello.sh`

```bash
#!/bin/sh
echo Hello World at `date`
```

We run the app in a container

```bash
docker run --rm  -v `pwd`/app:/app  alpine:latest /app/hello.sh
```

- `--rm` – make sure that the container is removed after each run
- as the app is bind-mounted into the containe we can modify the app and restart it for tests.

#### Dockerfile

We are very proud of or app and we want to containerize it. So we create a dockerfile

```dockerfile
FROM alpine:latest
COPY ./app /app
CMD /bin/sh /app/hello.sh
```

- `FROM` – use a base image
- `COPY` – copy the `./app` folder from the host to the images `/app` folder
- `CMD` – the command that is run when the container is started 

Then we create a new image:

```bash
docker build -t hellworld:v0.9 .
```

- `-t` – tag, visible in `docker image ls`
- `.`  tells Docker to look in the current directory for the `Dockerfile` and to use the current directory as a "context" so that we can reference files and directories from there.

We check the new image:

```bash
docker image ls
REPOSITORY                        TAG                  IMAGE ID            CREATED             SIZE
hellworld                         v0.9                 f92ccddb30ec        6 minutes ago       5.59MB
```

And of course we run the image

```bash
docker run hellworld:v0.9
```



### Dockerfile plus Python

Now we want to build an app that utilizes python. Of course we could use a python image as basis but for demo purposes we use a plain ubuntu image and add python.

Dockerfile 

```bash
FROM ubuntu:18.04
RUN apt-get update && apt-get -y install python
COPY ./app /app
CMD python /app/hello.py
```

Each instruction creates one layer:

- `FROM` creates a layer from the `ubuntu:18.04` Docker image.
- `RUN` installs python into your container
- `COPY` adds files from your Docker client’s current directory.
- `CMD` specifies what command to run within the container.

Build the image

```bash
docker build -t hellopython:v0.9 .
```

Run a container based on the image.

```bash
docker run hellopython:v0.9 
```

### ` docker build` in detail

```bash
docker build [OPTIONS] context
```

The `docker build` command builds Docker images from a Dockerfile and a “context”. 

#### Build Context

A build’s context is the set of files located in the specified `PATH` or `URL`. The build process can refer to any of the files in the context. For example, your build can use a [*COPY*](https://docs.docker.com/engine/reference/builder/#copy) instruction to reference a file in the context.

The `URL` parameter can refer to three kinds of resources: Git repositories, pre-packaged tarball contexts and plain text files.

The files in the build context are sent to the docker daemon which uses them for the build process.

#### Build Process

Each line (`FROM`, `RUN` , `COPY`,  `CMD` etc.) creates a new layer or intermediate image. This intermediate image is stored for later reuse.

Example:

- `FROM ubuntu:18.04` downloads the `ubuntu:18.04` from a repository and stores it locally.  

- `RUN apt-get update && apt-get -y install python` creates a new  layer or intermediate image based on the previous image, starts an intermediate container, executes `apt-get` in the container and installs python on the image. The intermediate container is then removed.

- `COPY` again creates a new  intermediate image based on the previous image and copies the requested files. To do so it may start an intermediate container.

- `CMD` creates the final image and defines the default command that is started when the container is started.

When the build process finds that layer / intermediate image has not changed, it is re-used. This speeds up the build process.

#### Layers

Each  layer or intermediate image represents the delta between the previus image and the current image. Layers are read only.

#### 

![Containers sharing same image](https://docs.docker.com/storage/storagedriver/images/sharing-layers.jpg)

When a container is started for an image, a thin read/write layer is added. Each container has its own read/write layer, so it has its own data.

Writing on a file in a container is based on a copy-on-write strategy. If the file exists in the read-only layers, a copy is created and then the copy is written.

Performance for write-heavy operations is not optimal. Storage for write-heavy operations should be located on volumes.

Data that must be shared between containers should also be located on volumes. Multiple containers can use the same volume and thus can share data.

### Dockerfile Details

- `FROM` 

  Base image `AS` imagename (`AS` imagename for multi-stage builds

-  `ARG` 

  Can define a variable that can be used later, i.e. in `FROM` 

  - `ARG CODE_VERSION=latest`
  - `FROM baseimage:${CODE_VERSION}`

- `RUN` 

  Runs a command in the build process

  - `RUN <command>` runs a command in the shell (`/bin/sh`)
  - `RUN ["executeable", "param1",  "param2"]` runs an executable
  - `RUN` creates a new layer, starts a container and and runs the command in the container. Results of the command are commited into a new image which is the basis for the next step of the build.

- `CMD` 

  Defines the command that runs when the container is started.

  - `CMD ["executable","param1","param2"]` (*exec* form, this is the preferred form)
  - `CMD ["param1","param2"]` (as *default parameters to ENTRYPOINT*)
  - `CMD command param1 param2` 

- ` ENTRYPOINT` 

  Allows you to configure a container that will run as an executable. It defines the command that runs when the container is started.

  - `ENTRYPOINT ["executable","param1","param2"]` (*exec* form, this is the preferred form)
  - `ENTRYPOINT ["param1","param2"]` 

  Rules

  - A `Dockerfile` should specify at least one of `CMD` or `ENTRYPOINT` commands.

  - `ENTRYPOINT` should be defined when using the container as an executable.
  - `CMD` should be used as a way of defining default arguments for an `ENTRYPOINT` command or for executing an ad-hoc command in a container.
  - `CMD` will be overridden when running the container with alternative arguments.

- `VOLUME`

  Defines a mount point for a container volume. `docker run` creates a volume and copies data from the container into the volume.

  ```bash
  FROM ubuntu
  RUN mkdir /myvol
  RUN echo "hello world" > /myvol/greeting
  VOLUME /myvol
  ```

- `LABEL` 

  Adds metadata to an image

  - `LABEL "com.example.vendor"="ACME Incorporated"`

- `EXPOSE` 

  Tells docker, that the container listens to a specific port. More or less a documentation feature for docker inspect. Can be overriden by `docker run -p ...`

- `ENV` allows to define environment variables

  - ```
    ENV DBNAME myDB
    ENV USERNAME Sepp
    ENV PASSWORD gehheim 
    ```

  - Each `ENV` step generates a new layer so it might be better to put all `ENV`steps in a single line

    ```bash
    ENV DBNAME=myDB USERNAME=Sepp PASSWORD=gehheim 
    ```

- `WORKDIR` 

  The `WORKDIR` instruction sets the working directory for any `RUN`, `CMD`, `ENTRYPOINT`, `COPY` and `ADD` instructions that follow it in the `Dockerfile`. If the `WORKDIR` doesn’t exist, it will be created even if it’s not used in any subsequent `Dockerfile` instruction.

- `COPY`

  The `COPY` instruction copies new files or directories from and adds them to the filesystem of the container at the path.  Copy only supports the basic copying of local files into the container

  COPY has two forms:

  - `COPY [--chown=:] ... `
  - `COPY [--chown=:] ["",... ""]` (this form is required for paths containing whitespace)

- `ADD`

  The `ADD` instruction copies new files or directories from and adds them to the filesystem of the container at the path.  `ADD` has some features (like local-only tar extraction and remote URL support) that are not immediately obvious.

  ADD has two forms:

  - `ADD [--chown=:] ... `
  - `ADD [--chown=:] ["",... ""]` (this form is required for paths containing whitespace)

 

## Docker Compose

See also https://docs.docker.com/compose/ and 

https://docs.docker.com/compose/compose-file

Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.

Example Wordpress installation

We need two services

- mysql 5.7 as a database service
  - data are persistent in a volume
- wordpress installation
  - depends on mysql
  - exposes port 8000
- Volumes for persistent data
- Isolated network for all services

Create a directory `wp` and `cd` into it.

```bash
mkdir wp
cd wp
```

Inside `wp` create a file `docker-compose.yml` or `docker-compose.yaml`. This file defines all services basd on docker images.

#### Commands

**Initialize containers and start them**

```bash
docker-compose up
```

Images are downloaded if necessary, containers are created and started. The shell waits for user input. Logs are shown so that we can see if something goes wrong. If it finds olf volumes it reuses them.

To stop, hit

```
<ctrl>C
```

**Initialize containers and start them in the background**

```bash
docker-compose up -d
```

This detaches the terminal from the containers.

**Shut containers down and remove them**

```bash
docker-compose down
```

**Start existing containers**

```bash
docker-compose start
```

**Start containers without removing them**

```bash
docker-compose stop
```

<br></br>

#### `docker-compose.yml`

```dockerfile
version: "3.3"

services:
  db:
    image: mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
      - db_log:/var/log/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    ports:
      - "8000:80"
    volumes:
      - type: bind
        source: ./web
        target: /var/www/html
        volume:
          nocopy: false
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress
volumes:
  db_data: {}
```

**Structure of `docker-compose.yml`**

- `version` Version of `docker-compose`
- `services:` Services that form an application and run together in an isolated envoronent. Think isolated network and isolated volumes. Each service runs a container based on an image.
  Each service can depend on another service. `wordpress` depends on the database service `db`.
- volumes: Volumes that are used by the services.

**A service inside `services:`**

- `depends_on:`
  - An array of services that our service depends on.
- `image:` the image that our service uses or alternatively
- `build: .` – when we have a Dockerfile in the current directory,  `docker-compose` builds the image.
- `ports:` a list of exposed ports
- `volumes:` a list of volumes and bind mounts
- `restart: `  `no`, `always`, `on-failure`, `unless-stopped` 
- `environment`: Array of environment variables that are used by the container.

**A volume inside`volumes:`**

**What happens, when we start `docker-compose up`**

- Isolated Containers
  - wp_wordpress_1
  - wp_db_1
- Isolated volumes
  - wp_db_data
- Isolated networks
  - wp_default

### `docker-compose` and build

We can combine `Dockerfile`s and `docker-compose.yml` in a folder `python`like so:

`Dockerfile`

```dockerfile
FROM ubuntu:18.04
RUN apt-get update
RUN apt-get -y install python
COPY ./app /app
CMD python /app/hello.py
```

`docker-compose.yml`

```yaml
version: "3.3"

services:
  hellopython:
    build: .
```

When we call `docker-compose up` for the first time,

- an image `python_hellopython` is built from the `Dockerfile`
- then a container `python_hellopython_1` is created and started.

Subsequent calls of  `docker-compose up`  start the container `python_hellopython_1` again.

When we need to change the app, we change the source and then call`docker-compose build` to build a new image and we start a container with  `docker-compose up`.

## Pushing to a Repository

An image can pe pushed to a docker repository. A Docker repository is a place where you can store 1 or more versions of a specific Docker image. An image can have 1 or more versions (tags). 

Note that a docker repository is not a Git repository. A Git repository is for source code, a Docker repository is for Docker images.

Multiple versions of an image go into one Docker repository.

hub.docker.com is a Docker registry. A Docker registry can contain many Docker repositories.

Repositories on hub.docker.com come in free and paid flavours.

### Using a Repository

- create an account at docker.com – you probably have that as you are using docker :-)

- Use your browser to log in into https://hub.docker.com

- You will see something similar to 

  ![docker-registry](images/docker-registry.png)

In this case

- `rhufsky` is the namespace in which your image will be visible. You also can create an _organization_ such as `4ahitn` and use that as namespace.

- Locally, create an image

  ```bash
  docker build -t rhufsky/basic:v0.9 .
  ```

- Log in locally

  ```bash
  docker login
  ```

- Push the image to the registry

  ```bash
  docker push rhufsky/basic:v0.9
  ```

- Refresh the broser and be proud

  ![docker-registry-with-first-repo](images/docker-registry-with-first-repo.png)

- Select the repository to see details

  ![docker-registry-with-first-repo-and-details](images/docker-registry-with-first-repo-and-details.png)

- When you create a new version (tag) of the image you can push it too.

  ```bash
  docker build -t rhufsky/basic:v1.0 .
  docker push rhufsky/basic:v1.0
  ```

- Then you can see it in the repository

  ![docker-registry-with-first-repo-and-details-plus-version](images/docker-registry-with-first-repo-and-details-plus-version.png)

- Anyone, who has access to this repo can pull one of these images and use them.

## App Development with Docker

Basically there are two strategies for app development.

- Develop and debug your app locally - relatively easy
  - Create an image with the necessary dependencies installed on your development system. For example:
    - Python, PHP or node.js
    - Python App with requirements.txt or
    - PHP App with composer.json or
    - Node.js App with package.json or
  - Create a Dockerfile that
    - creates an image
    - copies the dependecies description
    - installs the dependencies
    - runs the app
- Develop your app in a container - depends on your development environment, a bit more complex
  - Create an image with the necessary dependencies installed on your development system. For example:
    - Python, PHP or node.js
  - Bind-mount your sources into the container
  - Enable remote debugging
  - Configure your IDE so it uses the remote installation of Python, PHP or node.js.
  - Create configuration (Dockerfile, docker-compose.yml) that copies everything on a shippable container.



--

*For every problem there is one solution which is simple, neat, and wrong.*

*-- H. L. Mencken*