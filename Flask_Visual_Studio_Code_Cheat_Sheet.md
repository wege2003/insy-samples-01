# Flask & Visual Studio Code Cheat Sheet

## Create a Project

- macOS

  - if needed install python

    ```
    brew install python
    brew install python3-venv
    ```

  - Create project

    ```bash
    mkdir MyProject
    cd MyProject
    ```

  - Create virtual environment

    ```bash
    python3 -m venv env
    source env/bin/activate
    ```

  - Install flask and SQLalchemy

    ``` 
    pip3 install Flask-API Flask flask-restful flask_sqlalchemy  simplejson
    ```

     

  - Open folder `MyProject` in Visual Studio Code

  - Command - Palette -> Python: select interpreter 

    Select python from `env/bin`

  - Create `launch.json`:

    ```json
     		 {
      // Use IntelliSense to learn about possible attributes.
      // Hover to view descriptions of existing attributes.
      // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
      "version": "0.2.0",
      "configurations": [
        {
          "name": "Python: Flask",
          "type": "python",
          "request": "launch",
          "module": "flask",
          "env": {
            "FLASK_APP": "app.py",
            "FLASK_ENV": "development",
            "FLASK_DEBUG": "0"
          },
          "args": ["run", "--no-debugger", "--no-reload"],
          "jinja": true
        }
      ]
    }
    ```

  <br>

  <br>

  ## Working on an existing Project

  ### Clone the Repo

  ```bash
  git clone https://gitlab.htl-villach.at/IT-2019-2020-4AHITN/INSY-Samples.git
  ```

  ### Install Dependencies

  Change into the project folder

  ```bash
  cd INSY-Samples/P02-JSON
  ```

  Create and activate virtualenv

  ```bash
  python3 -m venv env
  source env/bin/activate
  ```

  Install depencencies

  ```bash
  pip3 install -r requirements.txt 
  ```

  Good to go!