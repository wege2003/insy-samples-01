from werkzeug.security import safe_str_cmp
from flask import Flask, jsonify, request, url_for
from flask_api import FlaskAPI, status, exceptions

from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity
)

from flask_bcrypt import Bcrypt

from database import initDatabase

# look at https://blog.tecladocode.com/learn-python-advanced-configuration-of-flask-jwt/

# from database import Message

app = FlaskAPI(__name__)

bcrypt = Bcrypt(app)

app.config['JWT_SECRET_KEY'] = 'Gehheimnis'  # Change this!
jwt = JWTManager(app)

db = initDatabase(app, "messageDB.sqlite")


def identity(payload):
    user_id = payload['identity']
    user = db.session.query(db.User).get(id)
    return user


@app.route('/api/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    user = db.session.query(db.User).filter(
        db.User.username == username).first()

    if not (user and bcrypt.check_password_hash(user.password, password)):
        return jsonify({"message": "Bad username or password"}), 401

    # Identity can be any data that is json serializable
    access_token = create_access_token(identity=user.id)
    refresh_token = create_refresh_token(identity=user.id)

    return jsonify(access_token=access_token, refresh_token=refresh_token), 200

# The jwt_refresh_token_required decorator insures a valid refresh
# token is present in the request before calling this endpoint. We
# can use the get_jwt_identity() function to get the identity of
# the refresh token, and use the create_access_token() function again
# to make a new access token for this identity.


@app.route('/api/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    userid = get_jwt_identity()
    ret = {
        'access_token': create_access_token(identity=userid)
    }
    return jsonify(ret), 200


@app.route('/api/messages/')
@jwt_required
def getMessages():
    messages = db.session.query(db.Message).all()
    return jsonify([m.serialize for m in messages])


@app.route("/api/messages/<int:id>")
@jwt_required
def getMessage(id):
    message = db.session.query(db.Message).get(id)
    if message:
        return jsonify(message.serialize)
    else:
        message = {"message": f'no message found for id {id}'}
        return message, status.HTTP_404_NOT_FOUND


@app.route('/api/messages/', methods=['POST'])
@jwt_required
def postMessage():
    message = db.Message(
        title=request.data["title"],
        content=request.data["content"],
        author=request.data["author"],
        user_id=request.data["user_id"]
    )
    db.session.add(message)
    db.session.commit()
    return {f"message": f'created message id {message.id}'}


@app.route('/api/messages/<int:id>', methods=['PUT'])
@jwt_required
def putMessage(id):
    message = db.session.query(db.Message).get(id)
    if message:
        message.title = request.data["title"]
        message.content = request.data["content"]
        message.author = request.data["author"]
        db.session.commit()
        return {"message": f'updated message id {message.id}'}
    else:
        message = {"message": f'no message found for id {id}'}
        return message, status.HTTP_404_NOT_FOUND


@app.route("/api/messages/<int:id>", methods=["DELETE"])
@jwt_required
def delMessage(id):
    message = db.session.query(db.Message).get(id)
    if message:
        db.session.delete(message)
        db.session.commit()
        return {"message": f'deleted message id {message.id}'}
    else:
        message = {"message": f'no message found for id {id}'}
        return message, status.HTTP_404_NOT_FOUND


@app.route("/api/users/<id>/messages", methods=["GET"])
@jwt_required
def getUserMessages(id):
    user = db.session.query(db.User).get(id)
    if user:
        messages = user.messages
        return jsonify([m.serialize for m in messages])
    else:
        message = {"message": f'no user found for id {id}'}
        return message, status.HTTP_404_NOT_FOUND

@app.errorhandler(403)
def errForbidden(e):
    message = {"message": f'forbidden'}
    return message, status.HTTP_403_FORBIDDEN


@app.errorhandler(404)
def errNotFound(e):
    message = {"message": f'API not found'}
    return message, status.HTTP_404_NOT_FOUND


@app.errorhandler(500)
def errInternal(e):
    print(e.original_exception)
    message = {"message": f'internal server error'}
    return message, status.HTTP_500_INTERNAL_SERVER_ERROR
