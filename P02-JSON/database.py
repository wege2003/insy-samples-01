from flask_sqlalchemy import SQLAlchemy, Model
from sqlalchemy import Column, String, Integer, ForeignKey

from flask_bcrypt import Bcrypt

##############################################################
# Initialize the database and create some basic data
##############################################################


def initDatabase(app, dbname):

#    app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///database/" + dbname
    app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://PythonDemo:gehheim@putre.htl-vil.local/PythonDemo"
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db = SQLAlchemy(app)
    bcrypt = Bcrypt(app)

    ##############################################################
    # Table messages / mapped class Message
    #
    # Many messages can be assiciated with one user
    ##############################################################

    class Message(db.Model):

        __tablename__ = 'messages'

        id = Column(Integer, primary_key=True)
        title = Column(String(250), nullable=False)
        content = Column(String(250), nullable=False)
        author = Column(String(250))

        user_id = Column(Integer, ForeignKey('users.id'))
        user = db.relationship("User", back_populates="messages")

        @property
        def serialize(self):
            return {
                'title': self.title,
                'content': self.content,
                'author': self.author,
                'id': self.id,
                'user_id': self.user_id,
            }

    ##############################################################
    # Table users / mapped class User
    #
    # One user can have many messages
    # One user can be in many departments
    ##############################################################

    class User(db.Model):

        __tablename__ = 'users'

        id = Column(Integer, primary_key=True)
        username = Column(String(250), nullable=False)
        password = Column(String(250), nullable=False)
        firstname = Column(String(250))
        lastname = Column(String(250))
        email = Column(String(250))

        messages = db.relationship("Message", back_populates="user")

        departments = db.relationship(
            "Department",
            secondary="userdepartment",
            back_populates="users")

        @property
        def serialize(self):
            return {
                'username': self.username,
                'firstname': self.firstname,
                'lastname': self.lastname,
                'email': self.email,
                'id': self.id,
            }

    ##############################################################
    # Table departments / mapped class Department
    #
    # One department can have many users
    ##############################################################

    class Department(db.Model):

        __tablename__ = "departments"

        id = Column(Integer, primary_key=True)
        shortname = Column(String(5), nullable=False)
        longname = Column(String(250), nullable=False)

        users = db.relationship(
            "User",
            secondary="userdepartment",
            back_populates="departments")

        @property
        def serialize(self):
            return {
                'shortname': self.shortname,
                'longname': self.longname,
                'id': self.id,
            }

    ##############################################################
    # Table userdepartent / mapped class UserDepartment
    #
    # Association table user <-> department
    ##############################################################

    class UserDepartment(db.Model):

        __tablename__ = "userdepartment"

        user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
        department_id = Column(Integer, ForeignKey(
            'departments.id'), primary_key=True)

    ##############################################################
    # Create all tables
    ##############################################################

    db.create_all()

    ##############################################################
    # Populate database
    ##############################################################

    msg = Message.query.all()
    ll = len(msg)

    if len(msg) <= 0 or True:

        hash = bcrypt.generate_password_hash("gehheim").decode("utf-8")

        # create two users
        sepp = User(firstname="Sepp", lastname="Schnorcher",
                    username="sepp", email="Sepp@schnorcher.com", password=hash)
        db.session.add(sepp)

        susi = User(firstname="Susi", lastname="Schnorcher",
                    username="susi", email="Susi@schnorcher.com", password=hash)
        db.session.add(susi)

        # create a few departments
        it = Department(shortname="IT", longname="Informationstechnologie")
        inf = Department(shortname="IF", longname="Informatik")
        bt = Department(shortname="BT", longname="Bautechnik")

        db.session.add(it)
        db.session.add(inf)
        db.session.add(bt)

        # associate a user with two departments
        sepp.departments = [it, inf]

        # associate a user with one department
        susi.departments = [bt]

        # add another department for the user
        susi.departments.append(it)

        # create a few messages and associate them with users
        for i in range(1, 10):
            message1 = Message(title=f"T{i}", content=f"C{i}", author=f"A{i}")
            if i % 2:
                message1.user = sepp
            else:
                message1.user = susi

            db.session.add(message1)

        # save everything to the database
        db.session.commit()

        # get one message by primary key
        aMessage = Message.query.get(1)

        # associate a new user with the message
        aMessage.user = susi

        # list all messages of one user
        susisMessages = susi.messages

        # list all departments of one user
        susisDepartments = susi.departments

        messages = Message.query.all()
        for m in messages:
            print (m.title, m.user.username)


        db.session.commit()

    ##############################################################
    # export all database classes
    ##############################################################

    db.Message = Message
    db.User = User
    db.Department = Department
    db.Userdepartment = UserDepartment

    return db
