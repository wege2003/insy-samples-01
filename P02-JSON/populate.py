

from database import session, Message, Base


messages = session.query(Message).all()

message = session.query(Message).filter_by(id=3).one()
message2 = session.query(Message).filter_by(author="author 2").one()

session.delete(message2)

if len(messages) <= 0:
    for i in range(1, 10):
        message = Message(
            title=f"title {i}", author=f"author {i}", content=f"content {i}")
        session.add(message)


session.commit()

