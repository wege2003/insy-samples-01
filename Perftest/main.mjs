import pkg from "@prisma/client";
const { PrismaClient } = pkg;

const prisma = new PrismaClient();

/**
 * Give me a random element of a list item
 * @param list
 */

const getRandomElement = (list) => {
  let randomId = Math.floor(list.length * Math.random());
  if (randomId === list.length) {
    // special case :-)
    randomId--;
  }
  return list[randomId];
};

/**
 * 90 % of all queries are indexed
 */
const shouldPerformIndexedQuery = () => {
  return Math.random() > 0.1;
};

console.log("Start");
try {

  // get all firstnames
  const distinctFirstnames = await prisma.$queryRaw(
    "SELECT distinct firstname FROM people;"
  );
  console.log(`Firstnames: ${distinctFirstnames.length}`);

  // get all lastnames
  const distinctLastnames = await prisma.$queryRaw(
    "SELECT distinct lastname FROM people;"
  );
  console.log(`Lastnames: ${distinctLastnames.length}`);

  for (let i = 0; i < 2000; i++) {
    // Start a new query every 150 ms
    setTimeout(() => {
      let query;
      let querytype;
      let queryvalue;

      if (shouldPerformIndexedQuery()) {
        const randomElement = getRandomElement(distinctLastnames);
        query = { where: { lastname: randomElement.lastname } };
        queryvalue = randomElement.lastname;
        querytype = "lastname";
      } else {
        const randomElement = getRandomElement(distinctFirstnames);
        query = { where: { firstname: randomElement.firstname } };
        queryvalue = randomElement.firstname;
        querytype = "firstname";
      }

      console.log(`Query ${querytype}: ${queryvalue} ${i}`);

      prisma.people
        .findMany(query)
        .then((result) => {
          console.log(
            `Finished ${querytype}: ${queryvalue} ${i} R: ${result.length}`
          );
        })
        .catch((err) => {
          console.error(`Failed ${querytype}: ${queryvalue} ${i}`);
          console.error(err);
        });
    }, 300 * i);
  }
  console.log("Loaded");
} catch (err) {
  console.error(err);
}
