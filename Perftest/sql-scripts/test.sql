-- long-running statement
SELECT 
 `PerfTestDB`.`people`.`id` ,
 `PerfTestDB`.`people`.`firstname` , 
 `PerfTestDB`.`people`.`lastname` ,
 `PerfTestDB`.`people`.`email` ,
 `PerfTestDB`.`people`.`phone` ,
 `PerfTestDB`.`people`.`gender` ,
 `PerfTestDB`.`people`.`address_id` ,
 `PerfTestDB`.`people`.`created_at` ,
 `PerfTestDB`.`people`.`updated_at` 
FROM `PerfTestDB`.`people` 
WHERE `PerfTestDB`.`people`.`lastname` = 'HONS';


-- MySQL reports only "normal" SQL statements here
select * from sys.`x$statements_with_runtimes_in_95th_percentile`;

-- 
SELECT * FROM performance_schema.events_statements_summary_by_digest;

-- show all prepared statements
SELECT * FROM performance_schema.prepared_statements_instances;
